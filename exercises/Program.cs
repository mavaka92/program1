﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;



namespace exercises
{
    class Program
    {
        static void Main(string[] args)
        {
          
         Console.Write("Spider name ");
         string name = (Console.ReadLine());
         Console.Write("Spider species ");
         string species = (Console.ReadLine());
         Console.Write("Enter number of legs ");
         int numberOfLegs = Convert.ToInt32(Console.ReadLine());
         Spider spider = new Spider(name, species, numberOfLegs);
         spider.ShowSpider(name, species, numberOfLegs);
         Console.ReadLine();
         



        }
    }
}
